package com.sample.bookstore.unit;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.sample.bookstore.domain.Book;
import com.sample.bookstore.repository.BookRepository;
import com.sample.bookstore.service.BookService;
import com.sample.bookstore.service.CategoryService;
import com.sample.bookstore.service.error.BookNotFoundException;

@RunWith(SpringRunner.class)
public class BookServiceTests {

    private BookService bookService;

    private final Long ISBN = 111111111L;
    
    private final String NAME = "test";
    
    private final Long ID = 1L;

    private Optional<Book> optionalBook;

    private Book result;

    @MockBean
    private BookRepository bookRepository;

    @MockBean
    private CategoryService categoryService;

    @Before
    public void setUp() {
	bookService = spy(new BookService(bookRepository, categoryService));
	result=new Book();
	result.setId(ID);
	result.setName(NAME);
	result.setIsbn(ISBN);
	optionalBook=Optional.of(result);
    }

    @Test
    public void getBookByIsbn() {
	when(bookRepository.findOneByIsbn(ISBN)).thenReturn(optionalBook);
	Book book=bookService.getBookByIsbn(ISBN);
	assert(book.getIsbn().equals(ISBN));
    }
    
    @Test
    public void getBookById() {
	when(bookRepository.findOne(ID)).thenReturn(result);
	Book book=bookService.getBookById(ID);
	assert(book.getId().equals(ID));
    }
    
    @Test
    public void getBookByName() {
	when(bookRepository.findOneByName(NAME)).thenReturn(optionalBook);
	Book book=bookService.getBookByName(NAME);
	assert(book.getName().equals(NAME));
    }
    
    
    @Test(expected=BookNotFoundException.class)
    public void getBookByIsbnNotFound() {
	when(bookRepository.findOneByIsbn(ISBN)).thenReturn(Optional.ofNullable(null));
	bookService.getBookByIsbn(ISBN);
    }
    
    @Test(expected=BookNotFoundException.class)
    public void getBookByIdNotFound() {
	when(bookRepository.findOne(ID)).thenReturn(null);	
	Book book=bookService.getBookById(ID);	
    }
    
    @Test(expected=BookNotFoundException.class)
    public void getBookByNameNotFound() {
	when(bookRepository.findOneByName(NAME)).thenReturn(Optional.ofNullable(null));
	Book book=bookService.getBookByName(NAME);
    }

}
