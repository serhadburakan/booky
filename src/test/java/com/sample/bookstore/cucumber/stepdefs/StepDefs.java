package com.sample.bookstore.cucumber.stepdefs;

import com.sample.bookstore.BookstoreApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = BookstoreApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
