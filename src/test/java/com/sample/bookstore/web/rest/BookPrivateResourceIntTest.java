package com.sample.bookstore.web.rest;

import com.sample.bookstore.BookstoreApp;
import com.sample.bookstore.domain.Book;
import com.sample.bookstore.domain.User;
import com.sample.bookstore.repository.BookRepository;
import com.sample.bookstore.repository.UserRepository;
import com.sample.bookstore.service.BookService;
import com.sample.bookstore.service.MailService;
import com.sample.bookstore.service.UserService;
import com.sample.bookstore.service.dto.BookDTO;
import com.sample.bookstore.web.rest.errors.ExceptionTranslator;
import com.sample.bookstore.web.rest.vm.ManagedUserVM;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UserResource REST controller.
 *
 * @see UserResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BookstoreApp.class)
public class BookPrivateResourceIntTest {

    private static final Long DEFAULT_ISBN = 99999999999L;
    
    private static final String DEFAULT_NAME = "TEST BOOK";
    

  
    @Autowired
    private BookService bookService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restBookMockMvc;

    private Book book;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        BookPrivateResource bookPrivateResource = new BookPrivateResource(bookService);
        this.restBookMockMvc = MockMvcBuilders.standaloneSetup(bookPrivateResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter)
            .build();
    }

    /**
     * Create a Book.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which has a required relationship to the User entity.
     */
    public static Book createEntity(EntityManager em) {
	Book book = new Book();
	book.setIsbn(DEFAULT_ISBN);
	book.setName(DEFAULT_NAME);
        return book;
    }

    @Before
    public void initTest() {
	book = createEntity(em);
    }

    @Test
    @Transactional
    public void createBook() throws Exception {
	PageRequest pr=new PageRequest(0, 100000000);
        int databaseSizeBeforeCreate = bookService.getBooks(pr).getContent().size();

       
        BookDTO bookDTO = new BookDTO();
        bookDTO.setIsbn(DEFAULT_ISBN);
        bookDTO.setName(DEFAULT_NAME);

        restBookMockMvc.perform(post("/book/private")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bookDTO)))
            .andExpect(status().isCreated());

        // Validate the User in the database
        List<Book> bookList =  bookService.getBooks(pr).getContent();
        assertThat(bookList).hasSize(databaseSizeBeforeCreate + 1);
        Book testBook = bookList.get(bookList.size() - 1);
        assertThat(testBook.getIsbn()).isEqualTo(DEFAULT_ISBN);
        assertThat(testBook.getName()).isEqualTo(DEFAULT_NAME);
        
    }

   
}
