package com.sample.bookstore.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sample.bookstore.domain.Book;
import com.sample.bookstore.domain.Category;



public interface BookRepository  extends JpaRepository<Book, Long>{
    
    Optional<Book> findOneByIsbn(Long isbn);

    Page<Book> findAll(Pageable pageable);

    Optional<Book> findOneByName(String name);

    Page<Book> findOneByCategory(Pageable pageable,Category catefory);

    @Query("select p from Book p join p.category category where category.name=:categoryName")
    Page<Book> findByCategoryName(Pageable pageable, @Param(value="categoryName") String categoryName);

  

}
