package com.sample.bookstore.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sample.bookstore.domain.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {

    Optional<Category> findOneByName(String name);

}
