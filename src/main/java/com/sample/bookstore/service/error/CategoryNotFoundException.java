package com.sample.bookstore.service.error;

public class CategoryNotFoundException  extends BaseException {

    public CategoryNotFoundException(String message) {
	super(message);
    }

}
