package com.sample.bookstore.service.error;

public class BookAlreadyExistsException extends BaseException {

    public BookAlreadyExistsException(String message) {
	super(message);
    }

}
