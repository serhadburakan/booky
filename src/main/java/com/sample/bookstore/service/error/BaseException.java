package com.sample.bookstore.service.error;

public class BaseException extends RuntimeException {

    public BaseException(String message){
        super(message);
    }

}