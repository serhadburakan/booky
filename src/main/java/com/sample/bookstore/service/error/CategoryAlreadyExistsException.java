package com.sample.bookstore.service.error;

public class CategoryAlreadyExistsException extends BaseException {

    public CategoryAlreadyExistsException(String message) {
	super(message);
    }
}
