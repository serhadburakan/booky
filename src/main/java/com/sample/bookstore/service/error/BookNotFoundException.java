package com.sample.bookstore.service.error;

public class BookNotFoundException extends BaseException {

    public BookNotFoundException(String message) {
	super(message);
	
    }

}
