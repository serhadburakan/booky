/**
 * View Models used by Spring MVC REST controllers.
 */
package com.sample.bookstore.web.rest.vm;
