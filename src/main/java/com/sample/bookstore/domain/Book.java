package com.sample.bookstore.domain;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.sample.bookstore.service.dto.BookDTO;

@Entity
@Table(name = "book")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Book extends AbstractAuditingEntity implements Serializable {

    		@Id 
    		@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    		@SequenceGenerator(name = "sequenceGenerator")
    		private Long id;
		
		@Size(max = 50)
		@Column(name = "name", length = 50)
		private String name;
		
		
		@Column(name = "isbn")
		private Long isbn;
		
		@ManyToOne
		private Category category;
		
		public Book() {
			   
		}
		

		public Book(BookDTO bookDTO) {
		   this.isbn=bookDTO.getIsbn();
		   this.name=bookDTO.getName();
		   
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Long getIsbn() {
			return isbn;
		}

		public void setIsbn(Long isbn) {
			this.isbn = isbn;
		}

		public Category getCategory() {
		    return category;
		}

		public void setCategory(Category category) {
		    this.category = category;
		}
		
		
		
}
